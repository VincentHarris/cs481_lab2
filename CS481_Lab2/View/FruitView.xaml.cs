﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CS481_Lab2.Model;
using CS481_Lab2.ViewModel;

namespace CS481_Lab2.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FruitView : ContentPage
    {
        FruitViewModel vm;
        public FruitView()
        {
            InitializeComponent();
            vm = new FruitViewModel();
            //listFruits.ItemsSource = vm.fruits;
            BindingContext = vm;
        }

        private void listFruits_ItemTapped(object sender, ItemTappedEventArgs e)
        {

        }
    }
}