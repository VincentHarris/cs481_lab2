﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481_Lab2.Model
{
    public class Fruit
    {
        public string FruitName { get; set; }
        public string Topic { get; set; }
        public string ShortDesc { get; set; }
        public string Image { get; set; }

        public List <Fruit> GetFruit()
        {
            List<Fruit> fruits = new List<Fruit>()
            {
                new Fruit()
                {
                    FruitName ="Banana", 
                    Topic="High Potassium", 
                    Image ="banana2.jpg", 
                    ShortDesc="Banana's are actually classified as a berry, they float in water and are one of the " +
                    "most popular fruit in the US."
                },

                new Fruit()
                {
                    FruitName ="Apple",
                    Topic="Keeps Doctor away",
                    Image ="apple.jpg",
                    ShortDesc="Whether or not it actually keeps doctors away is left to be determined."
                },

                new Fruit()
                {
                    FruitName ="Mango",
                    Topic="Looks like Crash Bandicoot's wumpa fruits",
                    Image ="mango.jpg",
                    ShortDesc="Eating 100 mangos won't give you an extra life, but it may end your current one."
                },



            };
            return fruits;
        }
    }
}
