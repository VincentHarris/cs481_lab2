﻿using CS481_Lab2.View;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Lab2
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new MainPage();
            MainPage = new FruitView();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
