﻿using System;
using System.Collections.Generic;
using System.Text;
using CS481_Lab2.Model;

namespace CS481_Lab2.ViewModel
{
    public class FruitViewModel
    {
        public List<Fruit> fruits { get; set; }

        public FruitViewModel()
        {
            fruits = new Fruit().GetFruit();
        }
    }
}
